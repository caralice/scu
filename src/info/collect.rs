pub mod battery;
pub mod drives;
pub mod graphics;
pub mod memory;
pub mod packages;
pub mod processor;
pub mod system;